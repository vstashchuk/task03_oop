package com.epam.Controller;

import com.epam.Model.Apartment;
import com.epam.Model.Mansion;
import com.epam.Model.PentHouse;
import com.epam.Model.RealEstateInt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ControllerClass implements ControllerInt {

    private List<RealEstateInt> mainArray = new ArrayList<>();

    {
        mainArray.add(new Mansion(190, 4, 4, 5, 57000, "Mansion", true));
        mainArray.add(new Apartment(100, 0, 1, 2, 25000, "Apartment", 5));
        mainArray.add(new PentHouse(120, 2, 2, 1, 35000, "Penthouse", 9, false));
        mainArray.add(new Apartment(95, 5, 3, 4, 23500, "Apartment", 2));
        mainArray.add(new Apartment(105, 1, 2, 2, 25000, "Apartment", 5));
        mainArray.add(new Mansion(215, 5, 5, 7, 50000, "Mansion", false));
        mainArray.add(new PentHouse(110, 1, 1, 5, 30000, "Penthouse", 9, true));
        mainArray.add(new Mansion(200, 6, 5, 5, 63000, "Mansion", true));
    }

    public Collection<RealEstateInt> sortByPrice() {
        return mainArray
                .stream()
                .sorted(Comparator.comparing(RealEstateInt::getPrice).reversed())
                .collect(Collectors.toList());
    }

    public Collection<RealEstateInt> sortBySchoolDistance() {
        return mainArray
                .stream()
                .sorted(Comparator.comparing(RealEstateInt::getDistToSchool))
                .collect(Collectors.toList());
    }

    public Collection<RealEstateInt> sortByGardenDistance() {
        return mainArray
                .stream()
                .sorted(Comparator.comparing(RealEstateInt::getDistToGarden))
                .collect(Collectors.toList());
    }

    public Collection<RealEstateInt> sortByPlaygroundDistance() {
        return mainArray
                .stream()
                .sorted(Comparator.comparing(RealEstateInt::getDistToPlayground))
                .collect(Collectors.toList());
    }

    public void showLowerThanPrice(int p_price){
        for(RealEstateInt estate : mainArray){
            if(estate.getPrice() < p_price) {
                System.out.println(estate.toString());
            }
        }
    }

    public void showLowerThanPlaygroundDistance(int p_distance){
        for(RealEstateInt estate : mainArray){
            if(estate.getDistToPlayground() < p_distance) {
                System.out.println(estate.toString());
            }
        }
    }

    public void showLowerThanGardenDistance(int p_distance){
        for(RealEstateInt estate : mainArray){
            if(estate.getDistToGarden() < p_distance) {
                System.out.println(estate.toString());
            }
        }
    }

    public void showLowerThanSchoolDistance(int p_distance){
        for(RealEstateInt estate : mainArray){
            if(estate.getDistToSchool() < p_distance) {
                System.out.println(estate.toString());
            }
        }
    }

    public void showRealEstateList(){
        for(RealEstateInt estate : mainArray){
            System.out.println(estate.toString());
        }
    }
}