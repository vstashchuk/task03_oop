package com.epam.Model;

public class PentHouse extends RealEstateInt{
    private boolean accessToRoof;
    private int apartFloor;

    public PentHouse() {}

    public PentHouse(double area, int distToPlayground, int distToGarden, int distToSchool, int price, String type, int apartFloor, boolean accessToRoof) {
        super(area, distToPlayground, distToGarden, distToSchool, price, type);
        this.accessToRoof = accessToRoof;
        setApartFloor(9);
    }

    public boolean isAccessToRoof() {
        return accessToRoof;
    }

    public void setAccessToRoof(boolean accessToRoof) {
        this.accessToRoof = accessToRoof;
    }

    public int getApartFloor() {
        return apartFloor;
    }

    public void setApartFloor(int apartFloor) {
        this.apartFloor = apartFloor;
    }

    @Override
    public String toString() {
        return "PentHouse" + "\n" +
                "Access To Roof = " + accessToRoof + ";\n" +
                "Apartment's Floor = " + apartFloor + ";\n"
                + super.toString();
    }
}
