package com.epam.Model;

public abstract class RealEstateInt {

    private double area;
    private int distToPlayground;
    private int distToGarden;
    private int distToSchool;
    private int price;
    private String type;



    public RealEstateInt() {
    }

    public RealEstateInt(double area, int distToPlayground, int distToGarden, int distToSchool, int price, String type) {
        this.area = area;
        this.distToPlayground = distToPlayground;
        this.distToGarden = distToGarden;
        this.distToSchool = distToSchool;
        this.price = price;
        this.type = type;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public int getDistToPlayground() {
        return distToPlayground;
    }

    public void setDistToPlayground(int distToPlayground) {
        this.distToPlayground = distToPlayground;
    }

    public int getDistToGarden() {
        return distToGarden;
    }

    public void setDistToGarden(int distToGarden) {
        this.distToGarden = distToGarden;
    }

    public int getDistToSchool() {
        return distToSchool;
    }

    public void setDistToSchool(int distToShcool) {
        this.distToSchool = distToShcool;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return  "Area = " + area + ";\n" +
                "Distance To Playground = " + distToPlayground + ";\n" +
                "Distance To Garden = " + distToGarden + ";\n" +
                "Distance To School = " + distToSchool + ";\n" +
                "Price = " + price + ";\n";
    }
}
