package com.epam.Model;

public class Mansion extends RealEstateInt {
    private boolean garage;

    public Mansion() {}

    public Mansion(double area, int distToPlayground, int distToGarden, int distToSchool, int price, String type, boolean garage) {
        super(area, distToPlayground, distToGarden, distToSchool, price, type);
        this.garage = garage;
    }

    public boolean isGarage() {
        return garage;
    }

    public void setGarage(boolean garage) {
        this.garage = garage;
    }

    @Override
    public String toString() {
        return "Mansion" + "\n" +
                "Garage=" + garage + ";\n"
                + super.toString();
    }
}