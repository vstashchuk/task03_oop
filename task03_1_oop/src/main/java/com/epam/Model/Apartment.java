package com.epam.Model;

public class Apartment extends RealEstateInt {
    private int apartFloor;

    public Apartment() {}

    public Apartment(double area, int distToPlayground, int distToGarden, int distToSchool, int price, String type, int apartFloor) {
        super(area, distToPlayground, distToGarden, distToSchool, price, type);
        this.apartFloor = apartFloor;
    }

    public int getApartFloor() {
        return apartFloor;
    }

    public void setApartFloor(int apartFloor) {
        this.apartFloor = apartFloor;
    }

    @Override
    public String toString() {
        return "Apartment" + "\n" +
                "Apartment Floor = " + apartFloor + ";\n"
                + super.toString();
    }
}