package com.epam.View;

import com.epam.Controller.*;

import java.util.Scanner;

public class ViewClass {

    private ControllerInt controlObj = new ControllerClass();

    public void showByPrice(int price) {
        controlObj.sortByPrice();
        controlObj.showLowerThanPrice(price);
    }

    public void showBySchoolDist(int dist) {
        controlObj.sortBySchoolDistance();
        controlObj.showLowerThanSchoolDistance(dist);
    }

    public void showByPlayDist(int dist) {
        controlObj.sortByPlaygroundDistance();
        controlObj.showLowerThanPlaygroundDistance(dist);
    }

    public void showByGardenDist(int dist) {
        controlObj.sortByGardenDistance();
        controlObj.showLowerThanGardenDistance(dist);
    }



    public void printMenu(){
        int option;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("App menu:");
            System.out.println("1. Show real estate list");
            System.out.println("2. Show real estate by wanted price");
            System.out.println("3. Show real estate by wanted distance to school");
            System.out.println("4. Show real estate by wanted distance to playground");
            System.out.println("5. Show real estate by wanted distance to garden");
            System.out.println("Other character. Quit program");
            System.out.print("Input a number to choose an option from menu: ");
            option = scanner.nextInt();
            System.out.println("\n");
            switch (option){
                case 1:
                    System.out.println("List of real estate:\n");
                    controlObj.showRealEstateList();
                    System.out.println("\n");
                    break;
                case 2:
                    System.out.println("Input max price:\n");
                    int maxPrice = scanner.nextInt();
                    controlObj.showLowerThanPrice(maxPrice);
                    System.out.println("\n");
                    break;
                case 3:
                    System.out.println("Input max distance to school:\n");
                    int maxSchoolDist = scanner.nextInt();
                    controlObj.showLowerThanSchoolDistance(maxSchoolDist);
                    System.out.println("\n");
                    break;
                case 4:
                    System.out.println("Input max playground distance:\n");
                    int maxPlayDist = scanner.nextInt();
                    controlObj.showLowerThanPlaygroundDistance(maxPlayDist);
                    System.out.println("\n");
                    break;
                case 5:
                    System.out.println("Input max distance to garden:\n");
                    int maxGardenDist = scanner.nextInt();
                    controlObj.showLowerThanGardenDistance(maxGardenDist);
                    System.out.println("\n");
                    break;
                default:
                    System.out.println("Bye");
                    System.out.println("\n");
            }

        }while(option == 1 || option == 2 || option == 3 || option == 4 || option == 5);
    }
}
