package com.epam.Controller;

import com.epam.Model.RealEstateInt;

import java.util.Collection;

public interface ControllerInt {

    Collection<RealEstateInt> sortByPrice();

    Collection<RealEstateInt> sortBySchoolDistance();

    Collection<RealEstateInt> sortByGardenDistance();

    Collection<RealEstateInt> sortByPlaygroundDistance();

    void showLowerThanPrice(int price);

    void showLowerThanPlaygroundDistance(int distance);

    void showLowerThanGardenDistance(int distance);

    void showLowerThanSchoolDistance(int distance);

    void showRealEstateList();
}
